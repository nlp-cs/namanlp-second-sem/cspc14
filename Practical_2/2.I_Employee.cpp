//
// Created by naman on 25/4/22.
//
// 2.I
//Write a program to create a class, Employee having the following attributes
// Private data:
//Employee_name, Employee_id, Employee_salary, Employee_address
//Public methods :
//getInput ();
//PrintOutput ();
//Write a main program to test it

#include <iostream>
#include <string>

class Employee{
private:
    std::string Employee_name;
    std::string Employee_id;
    std::string Employee_salary;
    std::string Employee_address;

public:
    void getInput(){
        std::cout << "Enter Employee name : ";
        std::cin >> Employee_name;
        std::cout << "Enter Employee id : ";
        std::cin >> Employee_id;
        std::cout << "Enter Employee salary : ";
        std::cin >> Employee_salary;
        std::cout << "Enter Employee address : ";
        std::cin >> Employee_address;
    }

    void PrintOutput(){
        std::cout << "========================================================================" << std::endl;
        std::cout << "Employee's name is : " << Employee_name << std::endl;
        std::cout << "Employee's id is : " << Employee_id << std::endl;
        std::cout << "Employee's salary is : " << Employee_salary << std::endl;
        std::cout << "Employee's address is : " << Employee_address << std::endl;
        std::cout << "========================================================================" << std::endl;
    }

};

int main(){
    int num = 0;
    std::cout << "Enter number of employees : " ;
    std::cin >> num;
    while (num--){
        Employee employee1;
        employee1.getInput();
        employee1.PrintOutput();
    }
}