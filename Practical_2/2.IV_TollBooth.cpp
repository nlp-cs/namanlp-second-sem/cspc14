//
// Created by naman on 25/4/22.
//
// Imagine a tollbooth at a bridge. Cars passing by the booth are expected to pay a fifty-
//cent toll. Mostly they do, but sometimes a car goes by without paying. The tollbooth keeps
//track of the number of cars that have gone by, and of the total amount of money collected.
//Model this tollbooth with a class called TollBooth. The two data items are a type unsigned int
//to hold the total number of cars, and a type double to hold the total number of cars, and a type
//double to hold the total amount of money collected. An initialization function sets both these
//values to 0. A member function called payingCar() increments the car total and adds 0.50 to
//the cash total. Another function, called nopayCar(), increments the car total but adds nothing
//to the cash total. Finally, a member function called display() desplays the two totals.
//Include a program to test this class. This program shold allow the user to push one key to count
//a paying car, and another to count a nonpaying car. Pusing the ESC key should cause the
//program to print out the total cars and total cash and then exit.

#include <iostream>

class TollBooth{
private:
    unsigned int numberOfCars;
    double moneyCollected;
public:

    TollBooth(){
        numberOfCars = 0;
        moneyCollected = 0;
    }

    void payingCar(){
        numberOfCars++;
        moneyCollected += 0.50;
    }
    void noPayCar(){
        numberOfCars++;
    }
    void display(){
        std::cout << "\nTotal number of cars : " << numberOfCars << "\nTotal Money collected : " << moneyCollected << std::endl;
    }
};

int main(){
    TollBooth booth;
    int choice = 0;
    while (choice != 4){
        std::cout << "========================================================================" << std::endl;
        std::cout  << " 1. Display" << "\n 2. Paying Car" << "\n 3. No Pay Car " << "\n 4. Quit" << std::endl << "Enter Your Choice : ";
        std::cin >> choice;
        switch (choice) {
            case 1: booth.display(); break;
            case 2: booth.payingCar(); break;
            case 3: booth.noPayCar(); break;
            case 4: std::cout << "\nThank You"<< std::endl; break;
            default: std::cout << "\nWrong choice, try again"; break;
        }
    }
}