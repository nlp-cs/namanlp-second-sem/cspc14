#include<iostream>
#define SWAP(x, y, t) {t = x; x = y; y = t;}
using namespace std;
class Array
{ int *a;
    int len;
public:
    Array(int l = 0);
    ~Array() {
        delete []a;}
    Array(Array &b);
    void display();
    int binarySearch(int item);
    void selectionSort();
};
Array::Array(int l)
{ len = l;
    a = new int[len + 1];
    for(int i = 0; i<len; i++)
    {
        cin>>a[i];
    }
}
Array::Array(Array &b)
{ len = b.len;
    a = new int[len + 1];
    for(int i = 0; i<len; i++)
    {
        a[i] = b.a[i];
    }
}
int Array::binarySearch(int item)
{
    int left, mid, right;
    left = 0;
    right = len - 1;
    while(left < right)
    {
        mid = (left + right) / 2;
        if(a[mid] == item)
        {
            return mid;
        }
        else if(a[mid] < item)
        {
            left = mid + 1;
        }
        else
        {
            right = mid - 1;
        }
    }
    return -1;
}
void Array::selectionSort()
{
    int i, j, pos, tmp;
    for(i = 0; i<len - 1; i++)
    {
        pos = i;
        for(j = i + i; j <len; j++)
        {
            if(a[pos] > a[j])
            {
                pos = j;
            }
        }
        if(pos != i)
        {
            SWAP(a[pos], a[i], tmp);
        }
    }
}
void Array::display()
{
    for(int i = 0; i<len; i++)
    {
        cout<<a[i]<<"\t";
    }
}
int main()
{
    int l, x;
    cout<<"\nEnter the number of elements in the Array : ";
    cin>>l;
    Array arr(l);
    cout<<"\nCopying this into another Array...\n";
    Array brr(arr);
    while(true)
    {
        cout<<"\n1. Sort 2. Search\n3. Display\n4. Exit\n";
        cin>>l;
        switch(l)
        {
            case 1: brr.selectionSort();
                break;
            case 2: cout<<"\nEnter the element to be searched for : ";
                cin>>x;
                x = brr.binarySearch(x);
                if(x != -1)
                {
                    cout<<"\nElement is present at index "<<x<<endl;
                }
                else
                {
                    cout<<"\nElement not present in the Array.\n";
                }
                break;
            case 3: brr.display();
                break;
            case 4: return 0;
            default:cout<<"\nWrong choice entered.\n";
        }
    }
}

