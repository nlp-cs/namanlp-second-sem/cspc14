//
// Created by naman on 5/6/22.
//

#include <iostream>
using namespace std;

class Stack{
private:
    int filled {};
    int array[100] {};

public:
    void push(int number){
        if (stackFull()){
            cout << "Sorry, Stack is already filled. Please remove a few elements first" << endl;
            return;
        }

        array[filled] = number;
        filled++;
    }

    int pop(){
        if (stackEmpty()){
            cout << "Sorry, Stack is already empty. Please add a few elements first" << endl;
            return -1;
        }

        cout << array[filled - 1] << " is removed from stack" << endl;
        array[filled-1] = 0;
        filled--;
    }

    int stackFull() const{
        if (filled >= 100)
            return 1;
        return 0;
    }
    int stackEmpty() const{
        if (filled == 0)
            return 1;
        return 0;
    }

    void printStack() const{
        cout << "Stack is : [ ";
        for (int i = 0; i < filled - 1 ; i++)
            cout  << array[i] << ", ";
        cout << array[filled-1] << " ]" << endl;
    }
};

int main(){
    Stack givenStack;
    int choice;
    do{
        cout << "==============================================================================" << endl;
        cout << "1 : Push an element into stack." << endl;
        cout << "2 : Pop an element from stack." << endl;
        cout << "3 : Check if stack is full." << endl;
        cout << "4 : Check if stack is empty." << endl;
        cout << "5 : Print Stack" << endl;
        cout << "6 : Quit" << endl;

        cout << "Enter Choice : ";
        cin >> choice;

        cout << "==============================================================================" << endl;

        switch (choice) {
            case 1: cout << "Enter Element : " ; int number; cin >> number; givenStack.push(number);  break;
            case 2: givenStack.pop(); break;
            case 3: cout<<  ( (givenStack.stackFull())? "Yes, it is full" : "No, it is not full" ) << endl; break;
            case 4: cout<<  ((givenStack.stackEmpty())? "Yes, it is empty" : "No, it is not empty") << endl; break;
            case 5: givenStack.printStack(); break;
            case 6: cout << "Thanks" << endl; break;
            default: cout << "Wrong Choice ! Please enter a valid choice;" << endl;
        }
    } while (choice != 6);
}