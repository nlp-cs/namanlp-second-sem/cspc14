//
// Created by naman on 9/5/22.
//

#include <iostream>
using namespace std;


class Fraction{

private:
    int denominator = 0;
    int numerator = 0;

public:
    Fraction(int n, int d){
        numerator = n;
        denominator = d;
    }
    Fraction operator -= (Fraction fr2){
        numerator = numerator * fr2.denominator - ( fr2.numerator * denominator );
        denominator = denominator * fr2.denominator;
    }

    static void displayFraction(Fraction fr){
        cout << fr.numerator << " / " << fr.denominator << endl;
    }
};

int main(){
    int d = 0, n = 0;

    cout << "Enter numerator and denominator for first fraction : " ;
    cin >> n >> d;
    Fraction fr1( n , d);
    cout << "Enter numerator and denominator for second fraction : " ;
    cin >> n >> d;
    Fraction fr2( n, d);
    fr1 -= fr2;
    cout << "Second Fraction subtracted from first gives : ";
    Fraction::displayFraction(fr1);
}