//
// Created by naman on 9/5/22.
//
// 4.I
//Create a class called Time that has separate int member data for hours, minutes, and
//seconds. One constructor should initialize this data to 0, and another should initialize it to fixed
//values. A member function should add two objects of type time passed as arguments. A main()
//program should create two initialized time objects, and one that isn’t initialized. The it should
//add the two initialized values together, leaving the result in third Time variable. Finally it
//should display the value of this third value.

#include <iostream>
using namespace std;

class Time{
private:
    int hours;
    int minutes;
    int seconds;
public:
    Time(){
        hours = 0;
        minutes=0;
        seconds=0;
    }
    Time(int hrs, int min, int sec){
        hours = hrs;
        minutes = min;
        seconds = sec;
    }

    static void addTime(Time * t3, Time t1, Time t2){
        t3->hours = t1.hours + t2.hours;
        t3->minutes = t1.minutes + t2.minutes;
        t3->seconds = t1.seconds + t2.seconds;
    }
    void display() const{
        cout << hours << " Hours, " << minutes << " Minutes and " << seconds << " seconds " << endl;
    };
};

int main(){
    int hrs = 0, min = 0, sec = 0;

    cout << "Enter hours, minutes and seconds for first object : " ;
    cin >> hrs >> min >> sec;
    Time time1(hrs, min, sec);
    cout << "Enter hours, minutes and seconds for second object : " ;
    cin >> hrs >> min >> sec;
    Time time2(hrs, min, sec), time3;

    Time::addTime(&time3, time1, time2);

    time3.display();
}