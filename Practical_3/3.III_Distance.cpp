//
// Created by naman on 2/5/22.
//
// 3.III Write a program that calculates the average of up to 100 English distances input by the
//user. Create an array of objects of the Distance class. To calculate the average, you can create
//dist_add() method, that perform addition over 100 distance’s object. You will also need a
//member function that divides a Distance value by an integer. A brief description of class
//Distance is given below.
//class Distance
//{
//private:
//int feet;float inches;
//public:
//void getDist();
//void showDist();
//};

#include <iostream>
using namespace std;

class Distance{
private:
    int feet {};
    float inches {};

public:
    void getDist(){
        cout << "Enter feet : ";
        cin >> feet;
        cout << "Enter inches : ";
        cin >> inches;

    }
    void showDist() const{
        cout << "Distance feet  : " << feet << endl;
        cout << "Distance inches : " << inches << endl;
    }
    void addSum (float * Feet, float * Inches) const{
        *Feet += ( float )feet;
        *Inches += inches;
    }

};

int main(){
    Distance distArray[100];
    int numberList = 0;
    cout << "Enter number of distances ( upto 100 ) : " ;
    cin >> numberList;
    for (int i =0 ; i<numberList;i++){
        cout << "Enter distance number  " << i+1 << endl;
        distArray[i].getDist();
    }
    cout << "=========================================================" << endl;
    float sumFeet {}, sumInch {};
    for (int i =0 ; i<numberList;i++){
        distArray[i].addSum(&sumFeet, &sumInch);
    }

    cout << "Average Feet : " << sumFeet / (float) numberList << endl;
    cout << "Average Inches : " << sumInch / (float) numberList << endl;
}

