//
// Created by naman on 18/04/22.
//

#include <iostream>

struct point{
    int  x = 0;
    int  y = 0;
};

int main(){
    point p1, p2, p3;
    std::cout << "Enter x and y coordinates of first point : " ;
    std::cin >> p1.x >> p1.y;
    std::cout << "Enter x and y coordinates of second point : " ;
    std::cin >> p2.x >> p2.y;
    p3.x = p1.x + p2.x;
    p3.y = p1.y + p2.y;
    std::cout << "Coordinates of third point are ( " << p3.x << " , " << p3.y << " )" << std::endl;

    return 0;
}